#!/bin/sh

podman container prune -f
podman image prune -f

cd ~/blueprints || exit 1

if git status >/dev/null 2>&1
then
	git pull
fi

find . -maxdepth 1 -mindepth 1 -type d -exec sh -c 'podman build -t "toy_$(basename "$1")" $1' shell {} \;
